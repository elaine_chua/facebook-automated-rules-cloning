<?php
/**
 * Author: Elaine
 * Email: elaine.chua@gmail.com
 * Created Date: 2020-08-06
 *
 */


	/* the rules you want to clone to */
	$app_secret_clone = "";
	$access_token_clone = "";
	$adaccount_id_clone = "";
	$scope_id_clone = "";
	$appsecret_proof_clone= hash_hmac('sha256', $access_token_clone, $app_secret_clone);

	//change v8.0 to your facebook app api version
	$url1 = "https://graph.facebook.com/v8.0/act_".$adaccount_id_clone."/adrules_library";


	/* the rules you want to clone from */
	$fields = "name,evaluation_spec,execution_spec,schedule_spec,status";
	$app_secret_master = "";
	$access_token_master = "";
	$adaccount_id_master  =  "";
	$appsecret_proof_master= hash_hmac('sha256', $access_token_master, $app_secret_master);

	//change v7.0 to your facebook app api version
	$url = "https://graph.facebook.com/v7.0/act_".$adaccount_id_master."/adrules_library?limit=500&fields=".$fields."&appsecret_proof=".$appsecret_proof_master."&access_token=".$access_token_master;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$url");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLVERSION , 6); //NEW ADDITION
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result_post = curl_exec($ch);
        curl_close($ch);

	$json = json_decode($result_post,true);

	if (isset($json["data"]))
	{
		$mydata = $json["data"];
		$i=0;
		foreach ($mydata as $mydata_result):
			$i++;
			$mydata_result["execution_spec"]["execution_options"]["0"]["value"]["0"] = $scope_id_clone;
			$r_name = $mydata_result["name"];
			$r_evaluation_spec = json_encode($mydata_result["evaluation_spec"]);
			$r_execution_spec = json_encode($mydata_result["execution_spec"]);
			$r_schedule_spec = json_encode($mydata_result["schedule_spec"]);
			$r_status = $mydata_result["status"];

			$postfields = "execution_spec=".$r_execution_spec."&evaluation_spec=".$r_evaluation_spec."&schedule_spec=".$r_schedule_spec."&name=".$r_name."&status=".$r_status."&appsecret_proof=".$appsecret_proof_clone."&access_token=".$access_token_clone;

			$ch1 = curl_init();
		        curl_setopt($ch1, CURLOPT_URL, "$url1");
                     	curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch1, CURLOPT_SSLVERSION , 6); //NEW ADDITION
			curl_setopt($ch1, CURLOPT_POST, 1);
			curl_setopt($ch1, CURLOPT_POSTFIELDS, $postfields);
		        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                	curl_setopt($ch1, CURLOPT_FOLLOWLOCATION, 1);
		        $result_post1 = curl_exec($ch1);
                        curl_close($ch1);

			print_r($result_post1);
		endforeach;
	}
?>
