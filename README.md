# Facebook Automated Rules Cloning
This project is to show you how to use php language to clone from existing facebook automated rules from one adaccount to another adaccount. 

# Installation
1. copy clone_rules.php to your server.
2. Editing clone_rules.php
<pre>
/* the rules you want to clone to */
$app_secret_clone = "";
$access_token_clone = "";
$adaccount_id_clone = "";
$scope_id_clone = "";

/* change v8.0 to your facebook app api version */
$url1 = "https://graph.facebook.com/v8.0/act_".$adaccount_id_clone."/adrules_library"; 

/* the rules you want to clone from */
$app_secret_master = "";
$access_token_master = "";
$adaccount_id_master  =  "";

/* change v7.0 to your facebook app api version */
$url = "https://graph.facebook.com/v7.0/act_".$adaccount_id_master."/adrules_library?limit=500&fields=".$fields."&appsecret_proof=".   $appsecret_proof_master."&access_token=".$access_token_master;
</pre>

# Youtube Link
https://youtu.be/UddcNgvlsvw

# Run
run your php script using "php clone_rules.php"